<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:25 PM
 */
return [
    'rest' => [
        'client_options' => [
            'cookies' => true,
            'timeout' => 5,
        ],
    ],
];
