<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/7/17
 * Time: 7:32 AM
 */

namespace Tests\Smorken\Rest\unit\Parsers;

use PHPUnit\Framework\TestCase;
use Smorken\Rest\Parsers\Unflatten;
use Smorken\Rest\ResponseException;

class UnflattenTest extends TestCase
{

    public function testNoOptionsThrowsException()
    {
        $sut = $this->getSut();
        $this->expectException(ResponseException::class);
        $this->expectExceptionMessage('Unflatten requires options set');
        $sut->parse([]);
    }

    public function testSimpleFromArray()
    {
        $sut = $this->getSut();
        $sut->setOptions(['identifier' => 'id']);
        $response = [
            ['id' => 1, 'foo' => 'bar 1'],
            ['id' => 2, 'foo' => 'bar 2'],
            ['id' => 3, 'foo' => 'bar 3'],
        ];
        $expected = [
            1 => ['id' => 1, 'foo' => 'bar 1'],
            2 => ['id' => 2, 'foo' => 'bar 2'],
            3 => ['id' => 3, 'foo' => 'bar 3'],
        ];
        $r = $sut->parse($response);
        $this->assertEquals($expected, $r);
    }

    public function testSimpleFromJson()
    {
        $sut = $this->getSut();
        $sut->setOptions(['identifier' => 'id']);
        $response = json_encode(
            [
                ['id' => 1, 'foo' => 'bar 1'],
                ['id' => 2, 'foo' => 'bar 2'],
                ['id' => 3, 'foo' => 'bar 3'],
            ]
        );
        $expected = [
            1 => ['id' => 1, 'foo' => 'bar 1'],
            2 => ['id' => 2, 'foo' => 'bar 2'],
            3 => ['id' => 3, 'foo' => 'bar 3'],
        ];
        $r = $sut->parse($response);
        $this->assertEquals($expected, $r);
    }

    public function testMultiKeySimpleFromArray()
    {
        $sut = $this->getSut();
        $sut->setOptions(['identifier' => ['id', 'id2']]);
        $response = [
            ['id' => 1, 'id2' => 'foo', 'foo' => 'bar 1'],
            ['id' => 2, 'id2' => 'foo', 'foo' => 'bar 2'],
            ['id' => 3, 'id2' => 'foo', 'foo' => 'bar 3'],
        ];
        $expected = [
            '1foo' => ['id' => 1, 'id2' => 'foo', 'foo' => 'bar 1'],
            '2foo' => ['id' => 2, 'id2' => 'foo', 'foo' => 'bar 2'],
            '3foo' => ['id' => 3, 'id2' => 'foo', 'foo' => 'bar 3'],
        ];
        $r = $sut->parse($response);
        $this->assertEquals($expected, $r);
    }

    public function testSimpleWithChildrenFromArray()
    {
        $sut = $this->getSut();
        $sut->setOptions(
            [
                'identifier' => 'id',
                'children'   => [
                    'child1' => [
                        'identifier' => 'child1_id',
                    ],
                    'child2' => [
                        'identifier' => 'child2_id',
                        'children'   => [
                            'child3' => [
                                'identifier' => 'child3_id',
                            ],
                        ],
                    ],
                ],
            ]
        );
        $response = [
            [
                'id'         => 1,
                'foo'        => 'bar 1',
                'child1_id'  => 'c1-1',
                'child1_foo' => 'c1 bar 1',
                'child2_id'  => 'c2-1',
                'child2_foo' => 'c2 bar 1',
                'child3_id'  => 'c3-1',
                'child3_foo' => 'c3 bar 1',
            ],
            [
                'id'         => 1,
                'foo'        => 'bar 2',
                'child1_id'  => 'c1-2',
                'child1_foo' => 'c1 bar 2',
                'child2_id'  => 'c2-2',
                'child2_foo' => 'c2 bar 1',
                'child3_id'  => 'c3-2',
                'child3_foo' => 'c3 bar 2',
            ],
            [
                'id'         => 1,
                'foo'        => 'bar 3',
                'child1_id'  => 'c1-3',
                'child1_foo' => 'c1 bar 3',
                'child2_id'  => 'c2-3',
                'child2_foo' => 'c2 bar 3',
                'child3_id'  => 'c3-3',
                'child3_foo' => 'c3 bar 3',
            ],
        ];
        $expected = [
            1 => [
                'id'         => 1,
                'foo'        => 'bar 1',
                'child1_id'  => 'c1-1',
                'child1_foo' => 'c1 bar 1',
                'child2_id'  => 'c2-1',
                'child2_foo' => 'c2 bar 1',
                'child3_id'  => 'c3-1',
                'child3_foo' => 'c3 bar 1',
                'child1'     => [
                    'c1-1' => [
                        'id'         => 1,
                        'foo'        => 'bar 1',
                        'child1_id'  => 'c1-1',
                        'child1_foo' => 'c1 bar 1',
                        'child2_id'  => 'c2-1',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-1',
                        'child3_foo' => 'c3 bar 1',
                    ],
                    'c1-2' => [
                        'id'         => 1,
                        'foo'        => 'bar 2',
                        'child1_id'  => 'c1-2',
                        'child1_foo' => 'c1 bar 2',
                        'child2_id'  => 'c2-2',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-2',
                        'child3_foo' => 'c3 bar 2',
                    ],
                    'c1-3' => [
                        'id'         => 1,
                        'foo'        => 'bar 3',
                        'child1_id'  => 'c1-3',
                        'child1_foo' => 'c1 bar 3',
                        'child2_id'  => 'c2-3',
                        'child2_foo' => 'c2 bar 3',
                        'child3_id'  => 'c3-3',
                        'child3_foo' => 'c3 bar 3',
                    ],
                ],
                'child2'     => [
                    'c2-1' => [
                        'id'         => 1,
                        'foo'        => 'bar 1',
                        'child1_id'  => 'c1-1',
                        'child1_foo' => 'c1 bar 1',
                        'child2_id'  => 'c2-1',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-1',
                        'child3_foo' => 'c3 bar 1',
                        'child3'     => [
                            'c3-1' => [
                                'id'         => 1,
                                'foo'        => 'bar 1',
                                'child1_id'  => 'c1-1',
                                'child1_foo' => 'c1 bar 1',
                                'child2_id'  => 'c2-1',
                                'child2_foo' => 'c2 bar 1',
                                'child3_id'  => 'c3-1',
                                'child3_foo' => 'c3 bar 1',
                            ],
                        ],
                    ],
                    'c2-2' => [
                        'id'         => 1,
                        'foo'        => 'bar 2',
                        'child1_id'  => 'c1-2',
                        'child1_foo' => 'c1 bar 2',
                        'child2_id'  => 'c2-2',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-2',
                        'child3_foo' => 'c3 bar 2',
                        'child3'     => [
                            'c3-2' => [
                                'id'         => 1,
                                'foo'        => 'bar 2',
                                'child1_id'  => 'c1-2',
                                'child1_foo' => 'c1 bar 2',
                                'child2_id'  => 'c2-2',
                                'child2_foo' => 'c2 bar 1',
                                'child3_id'  => 'c3-2',
                                'child3_foo' => 'c3 bar 2',
                            ],
                        ],
                    ],
                    'c2-3' => [
                        'id'         => 1,
                        'foo'        => 'bar 3',
                        'child1_id'  => 'c1-3',
                        'child1_foo' => 'c1 bar 3',
                        'child2_id'  => 'c2-3',
                        'child2_foo' => 'c2 bar 3',
                        'child3_id'  => 'c3-3',
                        'child3_foo' => 'c3 bar 3',
                        'child3'     => [
                            'c3-3' => [
                                'id'         => 1,
                                'foo'        => 'bar 3',
                                'child1_id'  => 'c1-3',
                                'child1_foo' => 'c1 bar 3',
                                'child2_id'  => 'c2-3',
                                'child2_foo' => 'c2 bar 3',
                                'child3_id'  => 'c3-3',
                                'child3_foo' => 'c3 bar 3',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $r = $sut->parse($response);
        $this->assertEquals($expected, $r);
    }

    public function testMultiKeyWithChildrenFromArray()
    {
        $sut = $this->getSut();
        $sut->setOptions(
            [
                'identifier' => ['id', 'id2'],
                'children'   => [
                    'child1' => [
                        'identifier' => 'child1_id',
                    ],
                    'child2' => [
                        'identifier' => 'child2_id',
                        'children'   => [
                            'child3' => [
                                'identifier' => 'child3_id',
                            ],
                        ],
                    ],
                ],
            ]
        );
        $response = [
            [
                'id'         => 1,
                'id2'        => '2-1',
                'foo'        => 'bar 1',
                'child1_id'  => 'c1-1',
                'child1_foo' => 'c1 bar 1',
                'child2_id'  => 'c2-1',
                'child2_foo' => 'c2 bar 1',
                'child3_id'  => 'c3-1',
                'child3_foo' => 'c3 bar 1',
            ],
            [
                'id'         => 1,
                'id2'        => '2-1',
                'foo'        => 'bar 1',
                'child1_id'  => 'c1-1',
                'child1_foo' => 'c1 bar 1',
                'child2_id'  => 'c2-2',
                'child2_foo' => 'c2 bar 2',
                'child3_id'  => null,
                'child3_foo' => null,
            ],
            [
                'id'         => 1,
                'id2'        => '2-2',
                'foo'        => 'bar 2',
                'child1_id'  => 'c1-2',
                'child1_foo' => 'c1 bar 2',
                'child2_id'  => 'c2-2',
                'child2_foo' => 'c2 bar 1',
                'child3_id'  => 'c3-2',
                'child3_foo' => 'c3 bar 2',
            ],
            [
                'id'         => 1,
                'id2'        => '2-3',
                'foo'        => 'bar 3',
                'child1_id'  => 'c1-3',
                'child1_foo' => 'c1 bar 3',
                'child2_id'  => 'c2-3',
                'child2_foo' => 'c2 bar 3',
                'child3_id'  => 'c3-3',
                'child3_foo' => 'c3 bar 3',
            ],
        ];
        $expected = [
            '12-1' => [
                'id'         => 1,
                'id2'        => '2-1',
                'foo'        => 'bar 1',
                'child1_id'  => 'c1-1',
                'child1_foo' => 'c1 bar 1',
                'child2_id'  => 'c2-1',
                'child2_foo' => 'c2 bar 1',
                'child3_id'  => 'c3-1',
                'child3_foo' => 'c3 bar 1',
                'child1'     => [
                    'c1-1' => [
                        'id'         => 1,
                        'id2'        => '2-1',
                        'foo'        => 'bar 1',
                        'child1_id'  => 'c1-1',
                        'child1_foo' => 'c1 bar 1',
                        'child2_id'  => 'c2-1',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-1',
                        'child3_foo' => 'c3 bar 1',
                    ],
                ],
                'child2'     => [
                    'c2-1' => [
                        'id'         => 1,
                        'id2'        => '2-1',
                        'foo'        => 'bar 1',
                        'child1_id'  => 'c1-1',
                        'child1_foo' => 'c1 bar 1',
                        'child2_id'  => 'c2-1',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-1',
                        'child3_foo' => 'c3 bar 1',
                        'child3'     => [
                            'c3-1' => [
                                'id'         => 1,
                                'id2'        => '2-1',
                                'foo'        => 'bar 1',
                                'child1_id'  => 'c1-1',
                                'child1_foo' => 'c1 bar 1',
                                'child2_id'  => 'c2-1',
                                'child2_foo' => 'c2 bar 1',
                                'child3_id'  => 'c3-1',
                                'child3_foo' => 'c3 bar 1',
                            ],
                        ],
                    ],
                    'c2-2' => [
                        'id'         => 1,
                        'id2'        => '2-1',
                        'foo'        => 'bar 1',
                        'child1_id'  => 'c1-1',
                        'child1_foo' => 'c1 bar 1',
                        'child2_id'  => 'c2-2',
                        'child2_foo' => 'c2 bar 2',
                        'child3_id'  => null,
                        'child3_foo' => null,
                        'child3' => [],
                    ]
                ],
            ],
            '12-2' => [
                'id'         => 1,
                'id2'        => '2-2',
                'foo'        => 'bar 2',
                'child1_id'  => 'c1-2',
                'child1_foo' => 'c1 bar 2',
                'child2_id'  => 'c2-2',
                'child2_foo' => 'c2 bar 1',
                'child3_id'  => 'c3-2',
                'child3_foo' => 'c3 bar 2',
                'child1'     => [
                    'c1-2' => [
                        'id'         => 1,
                        'id2'        => '2-2',
                        'foo'        => 'bar 2',
                        'child1_id'  => 'c1-2',
                        'child1_foo' => 'c1 bar 2',
                        'child2_id'  => 'c2-2',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-2',
                        'child3_foo' => 'c3 bar 2',
                    ],
                ],
                'child2'     => [
                    'c2-2' => [
                        'id'         => 1,
                        'id2'        => '2-2',
                        'foo'        => 'bar 2',
                        'child1_id'  => 'c1-2',
                        'child1_foo' => 'c1 bar 2',
                        'child2_id'  => 'c2-2',
                        'child2_foo' => 'c2 bar 1',
                        'child3_id'  => 'c3-2',
                        'child3_foo' => 'c3 bar 2',
                        'child3'     => [
                            'c3-2' => [
                                'id'         => 1,
                                'id2'        => '2-2',
                                'foo'        => 'bar 2',
                                'child1_id'  => 'c1-2',
                                'child1_foo' => 'c1 bar 2',
                                'child2_id'  => 'c2-2',
                                'child2_foo' => 'c2 bar 1',
                                'child3_id'  => 'c3-2',
                                'child3_foo' => 'c3 bar 2',
                            ],
                        ],
                    ],
                ],
            ],
            '12-3' => [
                'id'         => 1,
                'id2'        => '2-3',
                'foo'        => 'bar 3',
                'child1_id'  => 'c1-3',
                'child1_foo' => 'c1 bar 3',
                'child2_id'  => 'c2-3',
                'child2_foo' => 'c2 bar 3',
                'child3_id'  => 'c3-3',
                'child3_foo' => 'c3 bar 3',
                'child1'     => [
                    'c1-3' => [
                        'id'         => 1,
                        'id2'        => '2-3',
                        'foo'        => 'bar 3',
                        'child1_id'  => 'c1-3',
                        'child1_foo' => 'c1 bar 3',
                        'child2_id'  => 'c2-3',
                        'child2_foo' => 'c2 bar 3',
                        'child3_id'  => 'c3-3',
                        'child3_foo' => 'c3 bar 3',
                    ],
                ],
                'child2'     => [
                    'c2-3' => [
                        'id'         => 1,
                        'id2'        => '2-3',
                        'foo'        => 'bar 3',
                        'child1_id'  => 'c1-3',
                        'child1_foo' => 'c1 bar 3',
                        'child2_id'  => 'c2-3',
                        'child2_foo' => 'c2 bar 3',
                        'child3_id'  => 'c3-3',
                        'child3_foo' => 'c3 bar 3',
                        'child3'     => [
                            'c3-3' => [
                                'id'         => 1,
                                'id2'        => '2-3',
                                'foo'        => 'bar 3',
                                'child1_id'  => 'c1-3',
                                'child1_foo' => 'c1 bar 3',
                                'child2_id'  => 'c2-3',
                                'child2_foo' => 'c2 bar 3',
                                'child3_id'  => 'c3-3',
                                'child3_foo' => 'c3 bar 3',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $r = $sut->parse($response);
        $this->assertEquals($expected, $r);
    }

    protected function getSut()
    {
        return new Unflatten();
    }
}