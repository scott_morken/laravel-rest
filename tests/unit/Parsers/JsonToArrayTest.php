<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 10:18 AM
 */

namespace Tests\Smorken\Rest\unit\Parsers;

use PHPUnit\Framework\TestCase;
use Smorken\Rest\Parsers\JsonToArray;

class JsonToArrayTest extends TestCase
{

    public function testNullIsNull()
    {
        $sut = $this->getSut();
        $this->assertNull($sut->parse(null));
    }

    public function testNonJsonStringIsException()
    {
        $sut = $this->getSut();
        $this->expectExceptionMessage('Unable to convert response to JSON: bar');
        $sut->parse('bar');
    }

    public function testJsonStringSimple()
    {
        $sut = $this->getSut();
        $orig = "foo bar";
        $this->assertEquals($orig, $sut->parse(json_encode($orig)));
    }

    public function testJsonStringArray()
    {
        $sut = $this->getSut();
        $orig = ['foo' => 'bar', 'biz' => ['buz', 'baz']];
        $this->assertEquals($orig, $sut->parse(json_encode($orig)));
    }

    public function testJsonStringErrorIsException()
    {
        $sut = $this->getSut();
        $json = "{'foo':'bar','biz':'buz}";//missing closing '
        $this->expectExceptionMessage('Unable to convert response to JSON: {\'foo\':\'bar\',\'biz\':\'buz}');
        $sut->parse($json);
    }

    protected function getSut()
    {
        return new JsonToArray();
    }
}