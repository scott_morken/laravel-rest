<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 11:09 AM
 */

namespace Tests\Smorken\Rest\unit;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Mockery as m;
use Psr\Http\Message\ResponseInterface;
use Smorken\Rest\Contracts\Rest\Client;
use Smorken\Rest\Model;

class ModelTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testFirstModelFromResponse()
    {
        $json = json_encode([['foo' => 'bar'], ['foo' => 'baz']]);
        $handler = $this->getMockHandler(
            [
                new Response(200, [], $json),
            ]
        );
        list($model, $client, $config) = $this->getSut($handler);
        $m = $model->newRequest()
                   ->endpoint('foo/bar')
                   ->get()
                   ->client($client)
                   ->first();
        $this->assertInstanceOf(Model::class, $m);
        $this->assertEquals('bar', $m->foo);
    }

    public function testAllModelsFromResponse()
    {
        $json = json_encode([['foo' => 'bar'], ['foo' => 'baz']]);
        $handler = $this->getMockHandler(
            [
                new Response(200, [], $json),
            ]
        );
        list($model, $client, $config) = $this->getSut($handler);
        $c = $model->newRequest()
                   ->endpoint('foo/bar')
                   ->get()
                   ->client($client)
                   ->all();
        $this->assertInstanceOf(Collection::class, $c);
        $this->assertEquals('bar', $c[0]->foo);
        $this->assertEquals('baz', $c[1]->foo);
    }

    public function testElapsed()
    {
        $json = json_encode([['foo' => 'bar'], ['foo' => 'baz']]);
        $handler = $this->getMockHandler(
            [
                new Response(200, [], $json),
            ]
        );
        list($model, $client, $config) = $this->getSut($handler);
        $c = $model->newRequest()
                   ->endpoint('foo/bar')
                   ->get()
                   ->client($client)
                   ->all();
        $this->assertInternalType('float', $model->getElapsed());
    }

    public function testGetLastRequest()
    {
        $json = json_encode([['foo' => 'bar'], ['foo' => 'baz']]);
        $handler = $this->getMockHandler(
            [
                new Response(200, [], $json),
            ]
        );
        list($model, $client, $config) = $this->getSut($handler);
        $c = $model->newRequest()
                   ->endpoint('foo/bar')
                   ->get()
                   ->saveLastRestRequest()
                   ->client($client)
                   ->all();
        $expected = [
            'request' => [
                'get',
                'foo/bar',
                [],
                'async' => false,
            ],
            'response' => [
                'status' => 200,
                'headers' => [],
                'body' => '[{"foo":"bar"},{"foo":"baz"}]',
            ],
        ];
        $this->assertEquals($expected, $model->getLastRestRequest());
    }

    protected function getMockHandler($responses = [])
    {
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        return $handler;
    }

    protected function getSut($handler = null)
    {
        $opts = [];
        if ($handler) {
            $opts['handler'] = $handler;
        }
        $client = new \Smorken\Rest\Guzzle\Client($opts);
        $config = m::mock(Repository::class);
        $config->shouldReceive("get")
               ->with('rest.client_options', [])
               ->andReturn([]);
        Model::setClientClass(Client::class);
        $model = new Model();
        $model->setConfig($config);
        return [$model, $client, $config];
    }
}