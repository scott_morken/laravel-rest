<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 11:30 AM
 */

namespace Tests\Smorken\Rest\unit\Guzzle;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    public function testEndpointNotFoundThrowsNotFoundException()
    {
        $handler = $this->getMockHandler(
            [
                new Response(404, []),
            ]
        );
        $client = $this->getSut($handler);
        $this->expectExceptionMessage("Client error: `GET foo/bar` resulted in a `404 Not Found` response");
        $r = $client->handle(['GET', 'foo/bar']);
    }

    public function testClientHandleReturnsBody()
    {
        $handler = $this->getMockHandler(
            [
                new Response(200, [], 'Foo bar'),
            ]
        );
        $client = $this->getSut($handler);
        $r = $client->handle(['GET', 'foo/bar']);
        $this->assertEquals('Foo bar', $r);
    }

    public function testSaveLastAddsInfoToLast()
    {
        $handler = $this->getMockHandler(
            [
                new Response(200, [], 'Foo bar'),
            ]
        );
        $client = $this->getSut($handler);
        $client->saveLast();
        $r = $client->handle(['GET', 'foo/bar']);
        $expected = [
            'request' => [
                'GET',
                'foo/bar',
                'async' => false,
            ],
            'response' => [
                'status' => 200,
                'headers' => [],
                'body' => 'Foo bar',
            ],
        ];
        $debug = $client->debugLastRestRequest();
        $this->assertEquals($expected, $debug);
    }

    protected function getMockHandler($responses = [])
    {
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        return $handler;
    }

    protected function getSut($handler = null)
    {
        $opts = [];
        if ($handler) {
            $opts['handler'] = $handler;
        }
        $client = new \Smorken\Rest\Guzzle\Client($opts);
        return $client;
    }
}