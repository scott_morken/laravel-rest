<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 9:31 AM
 */

namespace Tests\Smorken\Rest\unit\Guzzle;

use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\Rest\Contracts\RequestException;
use Smorken\Rest\Contracts\Rest\Client;
use Smorken\Rest\Contracts\Rest\Model;
use Smorken\Rest\Contracts\RequestMethodException;
use Smorken\Rest\Guzzle\Request;
use Smorken\Rest\Parsers\ParseNone;

class RequestTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testNoendpointIsException()
    {
        list($sut, $client) = $this->getSut();
        $this->expectException(RequestException::class);
        $sut->run();
    }

    public function testInvalidendpointIsException()
    {
        list($sut, $client) = $this->getSut();
        $this->expectException(RequestException::class);
        $sut->endpoint('Ö');
    }

    public function testBasicRequestIsGet()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', []];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->toRequestOptions()
        );
        $this->assertEquals(Request::QUERYSTRING, $sut->getDefaultParamType());
    }

    public function testBasicPost()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['post', 'foo', []];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->post()
                           ->toRequestOptions()
        );
        $this->assertEquals(Request::BODY, $sut->getDefaultParamType());
    }

    public function testBasicPut()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['put', 'foo', []];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->put()
                           ->toRequestOptions()
        );
        $this->assertEquals(Request::BODY, $sut->getDefaultParamType());
    }

    public function testBasicPatch()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['patch', 'foo', []];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->patch()
                           ->toRequestOptions()
        );
        $this->assertEquals(Request::BODY, $sut->getDefaultParamType());
    }

    public function testBasicDelete()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['delete', 'foo', []];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->delete()
                           ->toRequestOptions()
        );
        $this->assertEquals(Request::QUERYSTRING, $sut->getDefaultParamType());
    }

    public function testBadMethodIsException()
    {
        list($sut, $client) = $this->getSut();
        $this->expectException(RequestMethodException::class);
        $sut->endpoint('foo')
            ->dance()
            ->toRequestOptions();
    }

    public function testGetWithQueryStringParams()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', ['query' => ['fiz' => 'buz']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->param('fiz', 'buz')
                           ->toRequestOptions()
        );
    }

    public function testGetWithQueryStringParamsWithSpace()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', ['query' => ['fiz' => 'buz baz']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->param('fiz', 'buz baz')
                           ->toRequestOptions()
        );
    }

    public function testGetWithParamsInBodyPutsInQueryString()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', ['query' => ['fiz' => 'buz', 'bar' => 'baz']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->param('fiz', 'buz')
                           ->param('bar', 'baz', Request::BODY)
                           ->toRequestOptions()
        );
    }

    public function testPostWithDefaultAndSetParams()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['post', 'foo', ['form_params' => ['fiz' => 'buz', 'bar' => 'baz']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->post()
                           ->param('fiz', 'buz')
                           ->param('bar', 'baz', Request::BODY)
                           ->toRequestOptions()
        );
    }

    public function testPostWithMixedParams()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['post', 'foo', ['query' => ['fiz' => 'buz'], 'form_params' => ['bar' => 'baz']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->post()
                           ->param('fiz', 'buz', Request::QUERYSTRING)
                           ->param('bar', 'baz', Request::BODY)
                           ->toRequestOptions()
        );
    }

    public function testPostWithParamsArray()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['post', 'foo', ['query' => ['fiz' => 'buz'], 'form_params' => ['bar' => 'baz']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->post()
                           ->params(['fiz' => 'buz'], Request::QUERYSTRING)
                           ->params(['bar' => 'baz'], Request::BODY)
                           ->toRequestOptions()
        );
    }

    public function testPostAsJsonBody()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['post', 'foo', ['json' => ['fiz' => 'buz', 'bar' => 'baz']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->post()
                           ->json()
                           ->param('fiz', 'buz')
                           ->param('bar', 'baz', Request::BODY)
                           ->toRequestOptions()
        );
    }

    public function testGetWithHeader()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', ['headers' => ['Content-Type' => 'application/json']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->header('Content-Type', 'application/json')
                           ->toRequestOptions()
        );
    }

    public function testGetWithHeaders()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', ['headers' => ['Content-Type' => 'application/json']]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->headers(['Content-Type' => 'application/json'])
                           ->toRequestOptions()
        );
    }

    public function testGetWithOption()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', ['timeout' => 2]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->option('timeout', 2)
                           ->toRequestOptions()
        );
    }

    public function testRunPassesOptionsToConnection()
    {
        list($sut, $client) = $this->getSut($this->getModelMock());
        $expected = ['get', 'foo', ['timeout' => 2]];
        $client->shouldReceive('handle')
               ->once()
               ->with($expected)
               ->andReturn('response');
        $r = $sut->endpoint('foo')
                 ->option('timeout', 2)
                 ->run();
        $this->assertEquals(
            'response', $r
        );
    }

    public function testGetWithOptions()
    {
        list($sut, $client) = $this->getSut();
        $expected = ['get', 'foo', ['timeout' => 2]];
        $this->assertEquals(
            $expected, $sut->endpoint('foo')
                           ->options(['timeout' => 2])
                           ->toRequestOptions()
        );
    }

    public function testFirstWithoutModelIsException()
    {
        list($sut, $client) = $this->getSut();
        $client->shouldReceive('handle')
               ->never();
        $this->expectExceptionMessage('Model must be set');
        $sut->endpoint('foo')
            ->option('timeout', 2)
            ->first();
    }

    public function testAllWithoutModelIsException()
    {
        list($sut, $client) = $this->getSut();
        $client->shouldReceive('send')
               ->never();
        $this->expectExceptionMessage('Model must be set');
        $sut->endpoint('foo')
            ->option('timeout', 2)
            ->all();
    }

    public function testFirstWithoutResultIsNull()
    {
        $m = $this->getModelMock();
        list($sut, $client) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $client->shouldReceive('handle')
               ->once()
               ->with($expected)
               ->andReturn(null);
        $this->assertNull(
            $sut->endpoint('foo')
                ->option('timeout', 2)
                ->first()
        );
    }

    public function testFirstWithResultIsFirstModel()
    {
        $m = $this->getModelMock();
        list($sut, $client) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $client->shouldReceive('handle')
               ->once()
               ->with($expected)
               ->andReturn([['bar' => 'foo']]);
        $m->shouldReceive('newInstance')
          ->with(['bar' => 'foo'])
          ->andReturn('model');
        $this->assertEquals(
            'model', $sut->endpoint('foo')
                         ->option('timeout', 2)
                         ->first()
        );
    }

    public function testAllWithoutResultIsEmptyCollection()
    {
        $m = $this->getModelMock();
        list($sut, $client) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $client->shouldReceive('handle')
               ->once()
               ->with($expected)
               ->andReturn(null);
        $this->assertCount(
            0, $sut->endpoint('foo')
                   ->option('timeout', 2)
                   ->all()
        );
    }

    public function testAllWithoutResultIsCollection()
    {
        $m = $this->getModelMock();
        list($sut, $client) = $this->getSut($m);
        $expected = ['get', 'foo', ['timeout' => 2]];
        $client->shouldReceive('handle')
               ->once()
               ->with($expected)
               ->andReturn([['bar' => 'foo']]);
        $m->shouldReceive('newInstance')
          ->with(['bar' => 'foo'])
          ->andReturn('model1');
        $coll = $sut->endpoint('foo')
                    ->option('timeout', 2)
                    ->all();
        $this->assertInstanceOf(Collection::class, $coll);
        $this->assertCount(1, $coll);
        $this->assertEquals('model1', $coll->first());
    }

    protected function getModelMock($parsers = [], $result_key = null)
    {
        if (!$parsers) {
            $parsers[] = new ParseNone();
        }
        $model = m::mock(Model::class);
        $model->shouldReceive('getParsers')
              ->andReturn($parsers);
        $model->shouldReceive('getResultKey')
              ->andReturn($result_key);
        $model->shouldReceive('setLastRestRequest');
        $model->shouldReceive('setElapsed');
        $model->shouldReceive('getParserOptions')->andReturnNull();
        return $model;
    }

    protected function getSut($model = null)
    {
        $sut = new Request(\Smorken\Rest\Guzzle\Client::class);
        $client = m::mock(Client::class);
        $client->shouldReceive('debugLastRestRequest');
        $client->shouldReceive('async')->with(m::type('bool'));
        $sut->client($client);
        if ($model) {
            $sut->setModel($model);
        }
        return [$sut, $client];
    }
}