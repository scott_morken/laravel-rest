<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/17
 * Time: 8:31 AM
 */

namespace Tests\Smorken\Rest\integration\Parsers;

use PHPUnit\Framework\TestCase;
use Smorken\Rest\Parsers\QueryServiceResults;

class QueryServiceResultsTest extends TestCase
{


    public function testParseMultiple()
    {
        $json = $this->getJson();
        $sut = $this->getSut();
        $parsed = $sut->parse($json);
        $this->assertInternalType('array', $parsed);
        $this->assertCount(5, $parsed);
        $this->assertArrayHasKey('EMPLID', $parsed[0]);
        $this->assertArrayHasKey('INSTITUTION', $parsed[0]);
        $this->assertArrayHasKey('ACAD_CAREER', $parsed[0]);
        $this->assertArrayHasKey('STRM', $parsed[0]);
        $this->assertArrayHasKey('DESCR', $parsed[0]);
        $this->assertArrayHasKey('SFA_SAP_STAT_CALC', $parsed[0]);
        $this->assertArrayHasKey('SFA_SAP_STATUS', $parsed[0]);
    }

    public function testParseSingle()
    {
        $json = $this->getJson('single');
        $sut = $this->getSut();
        $parsed = $sut->parse($json);
        $this->assertInternalType('array', $parsed);
        $this->assertCount(1, $parsed);
        $this->assertArrayHasKey('EMPLID', $parsed[0]);
        $this->assertArrayHasKey('INSTITUTION', $parsed[0]);
        $this->assertArrayHasKey('ACAD_CAREER', $parsed[0]);
        $this->assertArrayHasKey('STRM', $parsed[0]);
        $this->assertArrayHasKey('DESCR', $parsed[0]);
        $this->assertArrayHasKey('SFA_SAP_STAT_CALC', $parsed[0]);
        $this->assertArrayHasKey('SFA_SAP_STATUS', $parsed[0]);
    }

    /**
     * @return \Smorken\Rest\Parsers\QueryServiceResults
     */
    protected function getSut()
    {
        return new QueryServiceResults();
    }

    protected function getJson($name = 'multiple')
    {
        return file_get_contents(sprintf('%s/../fixtures/queryservice.%s.json', __DIR__, $name));
    }
}