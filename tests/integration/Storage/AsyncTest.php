<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/13/17
 * Time: 10:45 AM
 */

namespace Tests\Smorken\Rest\integration\Storage;

use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Rest\Model;
use Smorken\Rest\Parsers\QueryServiceResults;
use Smorken\Storage\Contracts\WebService\PromiseContainer;
use Smorken\Storage\WebService\Guzzle\Promise\Handler;
use Tests\Smorken\Rest\integration\Guzzle\Traits\Common;

class AsyncTest extends TestCase
{

    use Common;

    public function testHandleAsyncCanReturnPromises()
    {
        $responses = [
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        list($sut, $cache) = $this->getSut();
        $cache->shouldReceive('get')
              ->once()
              ->with('testssmorkenrestintegrationstoragestub.getById.1', null)
              ->andReturn(null);
        $sut->setClient($client);
        $promises = $sut->handleAsync('getById', [1]);
        $this->assertInstanceOf(PromiseContainer::class, $promises);
        $this->assertInstanceOf(PromiseInterface::class, $promises->all());
        $this->assertEquals('getById', $promises->getName());
    }

    public function testHandleNotAsyncCanReturnResponse()
    {
        $responses = [
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        list($sut, $cache) = $this->getSut();
        $cache->shouldReceive('get')
              ->once()
              ->with('testssmorkenrestintegrationstoragestub.getByIdNotAsync.1', null)
              ->andReturn(null);
        $sut->setClient($client);
        $promises = $sut->handleAsync('getByIdNotAsync', [1]);
        $this->assertInstanceOf(PromiseContainer::class, $promises);
        $this->assertInstanceOf(Model::class, $promises->all());
        $this->assertEquals('getByIdNotAsync', $promises->getName());
    }

    public function testHandleAsyncCanReturnMultiplePromises()
    {
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson('queryservice.multiple')),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        list($sut, $cache) = $this->getSut();
        $cache->shouldReceive('get')
              ->once()
              ->with('testssmorkenrestintegrationstoragestub.getOther.some_name.some_value', null)
              ->andReturn(null);
        $sut->setClient($client);
        $promises = $sut->handleAsync('getOther', ['some_name', 'some_value']);
        $this->assertInstanceOf(PromiseContainer::class, $promises);
        $items = $promises->all();
        $this->assertCount(2, $items);
        $this->assertInstanceOf(PromiseInterface::class, $items['first']);
        $this->assertInstanceOf(PromiseInterface::class, $items['second']);
        $this->assertEquals('getOther', $promises->getName());
    }

    public function testHandleAsyncCanReturnSplitResults()
    {
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson('queryservice.multiple')),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        list($sut, $cache) = $this->getSut();
        $cache->shouldReceive('get')
              ->once()
              ->with('testssmorkenrestintegrationstoragestub.getOtherSplit.some_name.some_value', null)
              ->andReturn(null);
        $sut->setClient($client);
        $promises = $sut->handleAsync('getOtherSplit', ['some_name', 'some_value']);
        $this->assertInstanceOf(PromiseContainer::class, $promises);
        $items = $promises->all();
        $this->assertCount(2, $items);
        $this->assertInstanceOf(PromiseInterface::class, $items['first']);
        $this->assertInstanceOf(Collection::class, $items['second']);
        $this->assertInstanceOf(Model::class, $items['second']->first());
        $this->assertEquals('getOtherSplit', $promises->getName());
    }

    /**
     * @return \Smorken\Rest\Guzzle\Promises\Handler
     */
    protected function getHandler()
    {
        return new Handler();
    }

    /**
     * @return array
     */
    protected function getSut()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $c = m::mock(Repository::class);
        $sut = new Stub($m);
        $sut->setCacheRepository($c);
        return [$sut, $c];
    }
}