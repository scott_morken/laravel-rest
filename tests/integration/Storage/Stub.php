<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/13/17
 * Time: 10:46 AM
 */

namespace Tests\Smorken\Rest\integration\Storage;

use Smorken\Rest\Guzzle\Client;
use Smorken\Storage\WebService\Guzzle\Async;

class Stub extends Async
{

    protected $caching = [
        'getOtherSplit' => 2,
    ];

    /**
     * @var \Smorken\Rest\Guzzle\Client
     */
    protected $client;

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function getById($id)
    {
        return $this->getModel()
                    ->newRequest()
                    ->endpoint('foo/bar')
                    ->client($this->client)
                    ->param('id', $id)
                    ->saveLast()
                    ->async()
                    ->first();
    }

    public function getOther($name, $value)
    {
        $others = [];
        $others['first'] = $this->getModel()
                                ->newRequest()
                                ->endpoint('foo/bar')
                                ->client($this->client)
                                ->params(['name' => $name, 'value' => $value])
                                ->saveLast()
                                ->async()
                                ->all();
        $others['second'] = $this->getModel()
                                 ->newRequest()
                                 ->endpoint('foo/bar')
                                 ->client($this->client)
                                 ->params(['name' => $name, 'value' => $value])
                                 ->saveLast()
                                 ->async()
                                 ->all();
        return $others;
    }

    public function getOtherSplit($name, $value)
    {
        $others = [];
        $others['first'] = $this->getModel()
                                ->newRequest()
                                ->endpoint('foo/bar')
                                ->client($this->client)
                                ->params(['name' => $name, 'value' => $value])
                                ->saveLast()
                                ->async()
                                ->all();
        $others['second'] = $this->getModel()
                                 ->newRequest()
                                 ->endpoint('foo/bar')
                                 ->client($this->client)
                                 ->params(['name' => $name, 'value' => $value])
                                 ->saveLast()
                                 ->all();
        return $others;
    }

    public function getByIdNotAsync($id)
    {
        return $this->getModel()
                    ->newRequest()
                    ->endpoint('foo/bar')
                    ->client($this->client)
                    ->param('id', $id)
                    ->saveLast()
                    ->first();
    }
}