<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/13/17
 * Time: 8:02 AM
 */

namespace Tests\Smorken\Rest\integration\Guzzle\Promises;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Rest\Model;
use Smorken\Rest\Parsers\QueryServiceResults;
use Smorken\Storage\WebService\Guzzle\Promise\Handler;
use Tests\Smorken\Rest\integration\Guzzle\Traits\Common;
use Tests\Smorken\Rest\integration\Storage\Stub;

class HandlerTest extends TestCase
{

    use Common;

    public function testUnwrapNoFailures()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }
        $sut = $this->getSut();
        $keyed_results = $sut->unwrap($promises);
        $this->assertCount(1, $keyed_results);
        $results = $keyed_results['default'];
        $this->assertCount(3, $results);
        $this->assertInstanceOf(Collection::class, $results['multi']);
        $this->assertInstanceOf(Collection::class, $results['single']);
        $this->assertInstanceOf(Collection::class, $results['zero']);
        $this->assertCount(5, $results['multi']);
        $this->assertCount(1, $results['single']);
        $this->assertCount(0, $results['zero']);
    }

    public function testUnwrapWithContainerSetsName()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }
        $coll = new \Smorken\Storage\WebService\Promise\Container($promises);
        $coll->setName('myPromises');
        $sut = $this->getSut();
        $keyed_results = $sut->unwrap($coll);
        $this->assertCount(1, $keyed_results);
        $this->assertArrayHasKey('myPromises', $keyed_results);
    }

    public function testUnwrapWithCallbackReturnsCallback()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }
        $coll = new \Smorken\Storage\WebService\Promise\Container($promises);
        $coll->onResponse(
            function ($response) {
                return 'ABC';
            }
        );
        $sut = $this->getSut();
        $keyed_results = $sut->unwrap($coll);
        $this->assertCount(1, $keyed_results);
        $this->assertEquals('ABC', $keyed_results['default']);
    }

    public function testUnwrapNoFailuresPresetPromises()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }
        $sut = $this->getSut();
        $sut->addMany($promises);
        $keyed_results = $sut->unwrap();
        $this->assertCount(1, $keyed_results);
        $results = $keyed_results['default'];
        $this->assertInstanceOf(Collection::class, $results['multi']);
        $this->assertInstanceOf(Collection::class, $results['single']);
        $this->assertInstanceOf(Collection::class, $results['zero']);
        $this->assertCount(5, $results['multi']);
        $this->assertCount(1, $results['single']);
        $this->assertCount(0, $results['zero']);
    }

    public function testUnwrapWithFailureIsException()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(400, 'Error'),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }
        $sut = $this->getSut();
        $this->expectExceptionMessage('Client error: `GET foo/bar` resulted in a `400 Bad Request` response:');
        $results = $sut->unwrap($promises);
    }

    public function testSettleNoFailures()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }
        $sut = $this->getSut();
        $keyed_results = $sut->settle($promises);
        $results = $keyed_results['default'];
        $this->assertCount(3, $results);
        $this->assertInstanceOf(Collection::class, $results['multi']);
        $this->assertInstanceOf(Collection::class, $results['single']);
        $this->assertInstanceOf(Collection::class, $results['zero']);
        $this->assertCount(5, $results['multi']);
        $this->assertCount(1, $results['single']);
        $this->assertCount(0, $results['zero']);
    }

    public function testSettleWithFailure()
    {
        $log = m::mock(Log::class);
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(400, 'Error'),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }
        $sut = $this->getSut();
        $sut->setLogger($log);
        $log->shouldReceive('error')
            ->once()
            ->with(m::type(RequestException::class));
        $keyed_results = $sut->settle($promises);
        $results = $keyed_results['default'];
        $this->assertCount(2, $results);
        $this->assertInstanceOf(Collection::class, $results['multi']);
        $this->assertInstanceOf(Collection::class, $results['single']);
        $this->assertArrayNotHasKey('zero', $results);
        $this->assertCount(5, $results['multi']);
        $this->assertCount(1, $results['single']);
    }

    public function testUnwrapWithAsyncStorageNotAsyncCanReturnResponse()
    {
        $responses = [
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        list($s, $c) = $this->getStorage();
        $c->shouldReceive('get')
          ->once()
          ->with('testssmorkenrestintegrationstoragestub.getByIdNotAsync.1', null)
          ->andReturn(null);
        $s->setClient($client);
        $promises = $s->handleAsync('getByIdNotAsync', [1]);
        $sut = $this->getSut();
        $keyed_results = $sut->unwrap($promises);
        $this->assertArrayHasKey('getByIdNotAsync', $keyed_results);
        $this->assertInstanceOf(Model::class, $keyed_results['getByIdNotAsync']);
    }

    public function testUnwrapWithAsyncStorageMultiplePromises()
    {
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson('queryservice.multiple')),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        list($s, $c) = $this->getStorage();
        $c->shouldReceive('get')
          ->once()
          ->with('testssmorkenrestintegrationstoragestub.getOther.some_name.some_value', null)
          ->andReturn(null);
        $s->setClient($client);
        $promises = $s->handleAsync('getOther', ['some_name', 'some_value']);
        $sut = $this->getSut();
        $keyed_results = $sut->unwrap($promises);
        $this->assertArrayHasKey('getOther', $keyed_results);
        $results = $keyed_results['getOther'];
        $this->assertCount(2, $results);
        $this->assertInstanceOf(Collection::class, $results['first']);
        $this->assertCount(5, $results['first']);
        $this->assertInstanceOf(Collection::class, $results['second']);
        $this->assertCount(1, $results['second']);
    }

    public function testUnwrapWithAsyncStorageCanReturnSplitResults()
    {
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson('queryservice.multiple')),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        list($s, $c) = $this->getStorage();
        $c->shouldReceive('get')
          ->once()
          ->with('testssmorkenrestintegrationstoragestub.getOtherSplit.some_name.some_value', null)
          ->andReturn(null);
        $c->shouldReceive('put')
          ->once()
          ->with('testssmorkenrestintegrationstoragestub.getOtherSplit.some_name.some_value', m::type('array'), 2);
        $s->setClient($client);
        $promises = $s->handleAsync('getOtherSplit', ['some_name', 'some_value']);
        $sut = $this->getSut();
        $keyed_results = $sut->unwrap($promises);
        $this->assertArrayHasKey('getOtherSplit', $keyed_results);
        $results = $keyed_results['getOtherSplit'];
        $this->assertCount(2, $results);
        $this->assertInstanceOf(Collection::class, $results['first']);
        $this->assertCount(5, $results['first']);
        $this->assertInstanceOf(Collection::class, $results['second']);
        $this->assertCount(1, $results['second']);
    }

    public function testUnwrapWithAsyncStorageCanReturnSplitResultsFromCache()
    {
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson('queryservice.multiple')),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
        ];
        $client = $this->getClient(
            $responses
        );
        $results = ['foo' => 'bar'];
        list($s, $c) = $this->getStorage();
        $c->shouldReceive('get')
          ->once()
          ->with('testssmorkenrestintegrationstoragestub.getOtherSplit.some_name.some_value', null)
          ->andReturn($results);
        $promises = $s->handleAsync('getOtherSplit', ['some_name', 'some_value']);
        $sut = $this->getSut();
        $keyed_results = $sut->unwrap($promises);
        $expected = ['getOtherSplit' => $results];
        $this->assertEquals($expected, $keyed_results);
    }

    /**
     * @return array
     */
    protected function getStorage()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $c = m::mock(Repository::class);
        $sut = new Stub($m);
        $sut->setCacheRepository($c);
        return [$sut, $c];
    }

    /**
     * @return Handler
     */
    protected function getSut()
    {
        return new Handler();
    }
}