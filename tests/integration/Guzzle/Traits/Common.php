<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/13/17
 * Time: 8:02 AM
 */

namespace Tests\Smorken\Rest\integration\Guzzle\Traits;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Config\Repository;
use Mockery as m;
use Smorken\Rest\Contracts\Rest\Model;
use Smorken\Rest\Guzzle\Client;

trait Common
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    protected function getResponseJson($name = 'queryservice.multiple')
    {
        return file_get_contents(sprintf('%s/../../fixtures/%s.json', __DIR__, $name));
    }

    protected function getModel($model_cls)
    {
        /**
         * @var Model $m
         */
        $m = new $model_cls;
        $this->addConfigToModel($m);
        return $m;
    }

    protected function createResponse($code, $body, $headers = [])
    {
        return new Response($code, $headers, $body);
    }

    protected function getClient($responses = [])
    {
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        return new Client(['handler' => $handler]);
    }

    protected function addConfigToModel($m)
    {
        $c = m::mock(Repository::class);
        $c->shouldReceive('get')
          ->with('rest.client_options', [])
          ->andReturn([]);
        $m->setConfig($c);
    }
}