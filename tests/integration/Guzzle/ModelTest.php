<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/17
 * Time: 8:55 AM
 */

namespace Tests\Smorken\Rest\integration\Guzzle;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Smorken\Rest\Parsers\QueryServiceResults;
use function GuzzleHttp\Promise\unwrap;
use Tests\Smorken\Rest\integration\Guzzle\Traits\Common;

class ModelTest extends TestCase
{

    use Common;

    public function testGetBasicResponseFromRun()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $response = $m->newRequest()
                      ->endpoint('foo/bar')
                      ->client($client)
                      ->saveLast()
                      ->run();
        $this->assertInternalType('array', $response);
        $this->assertCount(5, $response);
    }

    public function testAddOptionAddsOptionToGuzzleRequest()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $response = $m->newRequest()
                      ->endpoint('foo/bar')
                      ->client($client)
                      ->option('auth', ['foouser', 'foopassword'])
                      ->saveLast()
                      ->run();
        $last = $m->getLastRestRequest();
        $expected = [
            'get',
            'foo/bar',
            [
                'auth' => [
                    'foouser',
                    'foopassword',
                ],
            ],
            'async' => false,
        ];
        $this->assertEquals($expected, $last['request']);
    }

    public function testGetBasicModelCollectionFromAll()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $response = $m->newRequest()
                      ->endpoint('foo/bar')
                      ->client($client)
                      ->saveLast()
                      ->all();
        $this->assertInstanceOf(Collection::class, $response);
        $this->assertCount(5, $response);
        $first = $response->first();
        $this->assertInstanceOf(\Smorken\Rest\Model::class, $first);
        $this->assertEquals(30000000, $first->EMPLID);
        $this->assertEquals('PROB', $first->SFA_SAP_STATUS);
    }

    public function testGetBasicModelFromFirst()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $first = $m->newRequest()
                   ->endpoint('foo/bar')
                   ->client($client)
                   ->saveLast()
                   ->first();
        $this->assertInstanceOf(\Smorken\Rest\Model::class, $first);
        $this->assertEquals(30000000, $first->EMPLID);
        $this->assertEquals('PROB', $first->SFA_SAP_STATUS);
    }

    public function testGetBasicModelCollectionFromAllIsEmpty()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
            ]
        );
        $response = $m->newRequest()
                      ->endpoint('foo/bar')
                      ->client($client)
                      ->saveLast()
                      ->all();
        $this->assertInstanceOf(Collection::class, $response);
        $this->assertCount(0, $response);
    }

    public function testGetBasicModelFromFirstIsNullWhenEmpty()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
            ]
        );
        $first = $m->newRequest()
                   ->endpoint('foo/bar')
                   ->client($client)
                   ->saveLast()
                   ->first();
        $this->assertNull($first);
    }

    public function testRunAsAsyncDefaults()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $promise = $m->newRequest()
                     ->endpoint('foo/bar')
                     ->client($client)
                     ->saveLast()
                     ->async()
                     ->run();
        $response = $promise->wait();
        $this->assertInternalType('array', $response);
        $this->assertCount(5, $response);
    }

    public function testAllAsAsyncDefaults()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $promise = $m->newRequest()
                     ->endpoint('foo/bar')
                     ->client($client)
                     ->saveLast()
                     ->async()
                     ->all();
        $response = $promise->wait();
        $this->assertInstanceOf(Collection::class, $response);
        $this->assertCount(5, $response);
        $first = $response->first();
        $this->assertInstanceOf(\Smorken\Rest\Model::class, $first);
        $this->assertEquals(30000000, $first->EMPLID);
        $this->assertEquals('PROB', $first->SFA_SAP_STATUS);
    }

    public function testRunStoresResponse()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $promise = $m->newRequest()
                     ->endpoint('foo/bar')
                     ->client($client)
                     ->saveLast()
                     ->async()
                     ->run();
        $response = $promise->wait();
        $last = $m->getLastRequest();
        $body = json_decode($last['response']['body'], true);
        $expected_request = [
            'get',
            'foo/bar',
            [],
            'async' => true,
        ];
        $this->assertEquals($expected_request, $last['request']);
        $this->assertEquals($body['data']['query']['rows'], $response);
    }

    public function testRunAsAsyncOverrideSuccess()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(200, $this->getResponseJson()),
            ]
        );
        $success = function (ResponseInterface $response) {
            return 'ABC';
        };
        $promise = $m->newRequest()
                     ->endpoint('foo/bar')
                     ->client($client)
                     ->saveLast()
                     ->async($success)
                     ->run();
        $response = $promise->wait();
        $this->assertEquals('ABC', $response);
    }

    public function testRunAsAsyncOverrideFailure()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $client = $this->getClient(
            [
                $this->createResponse(400, 'Error'),
            ]
        );
        $failure = function (RequestException $e) {
            return 'ABC';
        };
        $promise = $m->newRequest()
                     ->endpoint('foo/bar')
                     ->client($client)
                     ->saveLast()
                     ->async(null, $failure)
                     ->run();
        $response = $promise->wait();
        $this->assertEquals('ABC', $response);
    }

    public function testAllAsAsyncMultiplePopulatesModels()
    {
        $m = $this->getModel(\Smorken\Rest\Model::class);
        $m->addParserClass(QueryServiceResults::class);
        $responses = [
            'multi'  => $this->createResponse(200, $this->getResponseJson()),
            'single' => $this->createResponse(200, $this->getResponseJson('queryservice.single')),
            'zero'   => $this->createResponse(200, $this->getResponseJson('queryservice.zero')),
        ];
        $client = $this->getClient(
            $responses
        );
        $promises = [];
        foreach ($responses as $k => $v) {
            $promises[$k] = $m->newRequest()
                              ->endpoint('foo/bar')
                              ->client($client)
                              ->saveLast()
                              ->async()
                              ->all();
        }

        $results = unwrap($promises);
        $this->assertCount(3, $results);
        $this->assertInstanceOf(Collection::class, $results['multi']);
        $this->assertInstanceOf(Collection::class, $results['single']);
        $this->assertInstanceOf(Collection::class, $results['zero']);
        $this->assertCount(5, $results['multi']);
        $this->assertCount(1, $results['single']);
        $this->assertCount(0, $results['zero']);
    }
}