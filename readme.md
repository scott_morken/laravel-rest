## REST extension for Laravel 5+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

* Add the service provider to `config/app.php`
```
'providers' => [
    ...,
    \Smorken\Rest\ServiceProvider::class,
    ...
]
```

#### REST (Guzzle) - todo

Extend the necessary models as needed
