<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 9:28 AM
 */

namespace Smorken\Rest\Guzzle;

class Request extends \Smorken\Rest\Request implements \Smorken\Rest\Contracts\Rest\Request
{

    /**
     * @return array
     */
    public function toRequestOptions()
    {
        $options = [
            $this->getMethod(),
            $this->buildUri(),
            [],
        ];
        $headers = $this->getHeaders();
        $this->addOption('headers', $headers, $options);
        $body = $this->getBody();
        if (!is_null($body)) {
            $this->addOption($body[0], $body[1], $options);
        }
        $qs = $this->getParams(self::QUERYSTRING);
        if ($qs) {
            $this->addOption('query', $qs, $options);
        }
        foreach ($this->getOptions() as $k => $v) {
            $this->addOption($k, $v, $options);
        }
        return $options;
    }

    public function buildUri()
    {
        return $this->getEndpoint();
    }
}