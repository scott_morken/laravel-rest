<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 9:34 AM
 */

namespace Smorken\Rest\Guzzle;

use Psr\Http\Message\ResponseInterface;
use Smorken\Rest\ResponseException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Client extends \GuzzleHttp\Client implements \Smorken\Rest\Contracts\Rest\Client
{

    protected $last;

    protected $saveLast = false;

    protected $async = false;

    protected $modifiers = [];

    public function debugLastRestRequest()
    {
        return $this->last;
    }

    /**
     * @param                $key
     * @param array|callable $handler
     * @return mixed
     */
    public function modifier($key, $handler)
    {
        $this->modifiers[$key] = $handler;
    }

    /**
     * @param array $options
     * @return mixed
     */
    public function handle(array $options)
    {
        $this->resetLast();
        $this->addRequestToLast($options);
        $response = call_user_func_array([$this, $this->getRequestMethod()], $options);
        if ($response instanceof ResponseInterface) {
            $this->validateResponse($response);
            return (string)$response->getBody();
        }
        return $response;//promise
    }

    protected function resetLast()
    {
        $this->last = [
            'request'  => [],
            'response' => [],
        ];
    }

    public function addRequestToLast($request)
    {
        if ($this->shouldSaveLast()) {
            $this->last['request'] = $request;
            $this->last['request']['async'] = $this->isAsync();
        }
    }

    /**
     * @return bool
     */
    public function shouldSaveLast()
    {
        return $this->saveLast;
    }

    /**
     * Is request async?
     *
     * @return boolean
     */
    public function isAsync()
    {
        return $this->async;
    }

    protected function getRequestMethod()
    {
        return $this->isAsync() ? 'requestAsync' : 'request';
    }

    protected function validateResponse(ResponseInterface $response)
    {
        $this->addResponseToLast($response);
        //should never get here using the default guzzle middleware
        if ($response->getStatusCode() >= 400) {
            $this->responseToError($response);
        }
        return true;
    }

    public function addResponseToLast(ResponseInterface $response)
    {
        if ($this->shouldSaveLast()) {
            $this->last['response'] = [
                'status'  => $response->getStatusCode(),
                'headers' => $response->getHeaders(),
                'body'    => (string)$response->getBody(),
            ];
        }
    }

    protected function responseToError(ResponseInterface $response)
    {
        $error_code = $response->getStatusCode();
        $msg = $response->getReasonPhrase();
        switch ($error_code) {
            case 404:
                $e = new NotFoundHttpException($msg);
                break;
            case 401:
                $e = new AccessDeniedHttpException($msg);
                break;
            case 403:
                $e = new UnauthorizedHttpException($msg);
                break;
            default:
                $e = new ResponseException($msg, $error_code);
                break;
        }
        throw $e;
    }

    public function saveLast()
    {
        $this->saveLast = true;
    }

    /**
     * Set request as async
     *
     * @param bool $async
     * @return void
     */
    public function async($async = true)
    {
        $this->async = $async;
    }
}