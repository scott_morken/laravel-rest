<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 4:04 PM
 */

namespace Smorken\Rest\Contracts;

interface ResponseException extends \Smorken\Model\Contracts\WebService\ResponseException
{

}
