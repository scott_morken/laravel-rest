<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:56 PM
 */

namespace Smorken\Rest\Contracts\Rest;

use Illuminate\Contracts\Logging\Log;

interface Request extends \Smorken\Model\Contracts\WebService\Request
{

    const QUERYSTRING = 1;
    const BODY = 2;

    /**
     * @return string
     */
    public function getClientClass();

    /**
     * Reset information
     *
     * @return void
     */
    public function init();

    /**
     * Set the URL endpoint for the REST request - required unless ::useLast()
     *
     * @param string $endpoint
     * @return $this
     */
    public function endpoint($endpoint);

    /**
     * Returns URL endpoint
     *
     * @return string
     */
    public function getEndpoint();

    /**
     * Add middleware to client request
     *
     * @param                $key
     * @param callable|array $callable
     */
    public function modifier($key, $callable);

    /**
     * Returns middleware assigned to client request
     *
     * @return array
     */
    public function getModifiers();

    /**
     * GET
     *
     * @return $this
     */
    public function get();

    /**
     * POST
     *
     * @return $this
     */
    public function post();

    /**
     * PUT
     *
     * @return $this
     */
    public function put();

    /**
     * PATCH
     *
     * @return $this
     */
    public function patch();

    /**
     * DELETE
     *
     * @return $this
     */
    public function delete();

    /**
     * Sets a request method manually
     *
     * @param string $method
     * @return $this
     */
    public function setMethod($method);

    /**
     * Returns request method
     *
     * @return string
     */
    public function getMethod();

    /**
     * Adds a header
     *
     * @param string $header
     * @param string $value
     * @return
     */
    public function header($header, $value);

    /**
     * Adds multiple headers to the header array
     *
     * @param array $headers
     */
    public function headers($headers = []);

    /**
     * Returns the headers
     *
     * @return array
     */
    public function getHeaders();

    /**
     * Sets the logger instance, useful for testing
     *
     * @param Log $logger
     * @return $this
     */
    public function setLogger(Log $logger);

    /**
     * Store the last Rest request debug info on the model
     *
     * @return $this
     */
    public function saveLastRestRequest();

    /**
     * Short version of ::saveLastRestRequest()
     *
     * @return $this
     */
    public function saveLast();

    /**
     * Use the provided response instead of using a client request
     *
     * @param $result
     * @return $this
     */
    public function useLast($result);

    /**
     * Returns the user provided response
     *
     * @return mixed
     */
    public function getUseLast();

    /**
     * Set as async request
     *
     * @param callable|null $success
     * @param callable|null $failure
     * @return $this
     */
    public function async(callable $success = null, callable $failure = null);

    /**
     * Is request async?
     *
     * @return boolean
     */
    public function getAsync();

    /**
     * Sets a parameter by type (::QUERYSTRING, ::BODY)
     *
     * @param      $key
     * @param      $value
     * @param null $type
     * @return $this
     */
    public function param($key, $value, $type = null);

    /**
     * Adds multiple parameters
     *
     * @param array $params
     * @param null  $type
     * @return $this
     */
    public function params(array $params, $type = null);

    /**
     * Returns the selected parameters by type, all if type is null
     *
     * @param null $type
     * @return array
     */
    public function getParams($type = null);

    /**
     * Returns the default param type (based on method)
     *
     * @return int
     */
    public function getDefaultParamType();

    /**
     * Adds an option - options are generic key => values that are added
     * to the client at creation
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function option($key, $value);

    /**
     * Add multiple options
     *
     * @param $options
     * @return $this
     */
    public function options($options);

    /**
     * Return options array
     *
     * @return array
     */
    public function getOptions();

    /**
     * sets the json flag on the request
     *
     * @return $this
     */
    public function json();

    /**
     * Return whether the request should use a json body
     *
     * @return bool
     */
    public function getJson();

    /**
     * ['json' => params] or ['form_params' => params] or null if no body
     *
     * @return array|null
     */
    public function getBody();

    /**
     * Builds a URI endpoint with the provided information
     *
     * @return string
     */
    public function buildUri();

    /**
     * Builds an array of options to pass to the client
     *
     * @return array
     */
    public function toRequestOptions();

    /**
     * Sets the client to override the build process - useful for testing
     *
     * @param \Smorken\Rest\Contracts\Rest\Client $client
     * @return $this
     */
    public function client(Client $client);

    /**
     * Builds and/or returns the client
     *
     * @return Client
     */
    public function getClient();
}
