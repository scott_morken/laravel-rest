<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 5:17 PM
 */

namespace Smorken\Rest\Contracts\Rest;

use Psr\Http\Message\ResponseInterface;

interface Client
{

    /**
     * Runs the provided request
     *
     * @param array $options
     * @return mixed
     */
    public function handle(array $options);

    /**
     * Returns the debug information if available
     *
     * @return mixed
     */
    public function debugLastRestRequest();

    /**
     * Instructs the client to save the debug information
     *
     * @return void
     */
    public function saveLast();

    /**
     * Is the client saving the debug information?
     *
     * @return bool
     */
    public function shouldSaveLast();

    /**
     * Set request as async
     *
     * @param bool $async
     * @return void
     */
    public function async($async = true);

    /**
     * Is request async?
     *
     * @return boolean
     */
    public function isAsync();

    /**
     * Adds middleware to the client request
     *
     * @param                $key
     * @param array|callable $handler
     * @return mixed
     */
    public function modifier($key, $handler);

    public function addRequestToLast($request);

    public function addResponseToLast(ResponseInterface $response);
}
