<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/14/17
 * Time: 2:56 PM
 */

namespace Smorken\Rest\Contracts\Rest;

interface Model extends \Smorken\Model\Contracts\WebService\Model
{

    /**
     * Sets the client class (string)
     *
     * @param string $client_class
     * @return
     */
    public static function setClientClass($client_class);

    /**
     * The array key that will bring back the result set for this model
     *
     * @return null|string
     */
    public function getResultKey();

    /**
     * The array key that will bring back the result set for this model
     *
     * @param $key
     */
    public function setResultKey($key);

    /**
     * Sets the last request information on the model
     *
     * @param $data
     */
    public function setLastRestRequest($data);

    /**
     * Returns the last request information if available
     *
     * @return array|null
     */
    public function getLastRestRequest();

    /**
     * Returns the client class (string)
     *
     * @return string
     */
    public function getClientClass();
}
