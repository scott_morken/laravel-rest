<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 9:54 AM
 */

namespace Smorken\Rest;

use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Collection;
use Psr\Http\Message\ResponseInterface;
use Smorken\Rest\Contracts\ResponseException;

abstract class Request implements \Smorken\Rest\Contracts\Rest\Request
{

    /**
     * @var string
     */
    protected $clientClass;

    /**
     * @var \Smorken\Rest\Contracts\Rest\Client
     */
    protected $client;

    /**
     * @var \Smorken\Rest\Contracts\Rest\Model
     */
    protected $model;

    protected $method = 'get';

    protected $headers = [];

    protected $params = [];

    protected $client_options = [];

    protected $request_options = [];

    protected $modifiers = [];

    protected $json = false;

    protected $endpoint;

    protected $default_param_type = self::QUERYSTRING;

    protected $saveLast = false;

    protected $async = false;

    protected $async_funcs = [
        'success' => null,
        'failure' => null,
    ];

    protected $type = 'run';

    protected $last;

    /**
     * @var Log
     */
    protected $logger;

    protected $elapsed = [
        'start'   => 0,
        'client'  => 0,
        'request' => 0,
        'total'   => 0,
    ];

    public function __construct($clientClass)
    {
        $this->clientClass = $clientClass;
        $this->init();
    }

    public function init()
    {
        $this->client = null;
        $this->model = null;
        $this->method = 'get';
        $this->headers = [];
        $this->params = [];
        $this->client_options = [];
        $this->request_options = [];
        $this->modifiers = [];
        $this->json = false;
        $this->endpoint;
        $this->default_param_type = self::QUERYSTRING;
        $this->saveLast = false;
        $this->async = false;
        $this->async_funcs = [
            'success' => null,
            'failure' => null,
        ];
        $this->type = 'run';
        $this->last;
        $this->elapsed = [
            'start'   => 0,
            'client'  => 0,
            'request' => 0,
            'total'   => 0,
        ];
    }

    /**
     * @param $endpoint
     * @return $this
     * @throws RequestException
     */
    public function endpoint($endpoint)
    {
        $endpoint = filter_var($endpoint, FILTER_SANITIZE_URL);
        if (!$endpoint) {
            throw new RequestException('Invalid endpoint.');
        }
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * GET
     *
     * @return $this
     */
    public function get()
    {
        $this->method = 'get';
        $this->default_param_type = self::QUERYSTRING;
        return $this;
    }

    /**
     * POST
     *
     * @return $this
     */
    public function post()
    {
        $this->method = 'post';
        $this->default_param_type = self::BODY;
        return $this;
    }

    /**
     * PUT
     *
     * @return $this
     */
    public function put()
    {
        $this->method = 'put';
        $this->default_param_type = self::BODY;
        return $this;
    }

    /**
     * PATCH
     *
     * @return $this
     */
    public function patch()
    {
        $this->method = 'patch';
        $this->default_param_type = self::BODY;
        return $this;
    }

    /**
     * DELETE
     *
     * @return $this
     */
    public function delete()
    {
        $this->method = 'delete';
        $this->default_param_type = self::QUERYSTRING;
        return $this;
    }

    /**
     * Handles invalid method calls to avoid having an uncatchable error
     *
     * @param $key
     * @param $params
     * @throws RequestMethodException
     */
    public function __call($key, $params)
    {
        throw new RequestMethodException("$key is not a valid method.");
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function headers($headers = [])
    {
        foreach ($headers as $key => $value) {
            $this->header($key, $value);
        }
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function header($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * @param array $params
     * @param null  $type
     * @return $this
     */
    public function params(array $params, $type = null)
    {
        foreach ($params as $k => $v) {
            $this->param($k, $v, $type);
        }
        return $this;
    }

    /**
     * @param      $key
     * @param      $value
     * @param null $type
     * @return $this
     */
    public function param($key, $value, $type = null)
    {
        $this->params[$key] = ['value' => $value, 'type' => $type];
        return $this;
    }

    /**
     * @param $options
     * @return $this
     */
    public function options($options)
    {
        foreach ($options as $k => $v) {
            $this->option($k, $v);
        }
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function option($key, $value)
    {
        $this->request_options[$key] = $value;
        return $this;
    }

    /**
     * sets the json flag on the request
     *
     * @return $this
     */
    public function json()
    {
        $this->json = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function saveLast()
    {
        return $this->saveLastRestRequest();
    }

    /**
     * Store the last soap request debug info on the model
     *
     * @return $this;
     */
    public function saveLastRestRequest()
    {
        $this->saveLast = true;
        return $this;
    }

    public function useLast($request)
    {
        $this->last = $request;
        return $this;
    }

    /**
     * @param                $key
     * @param callable|array $callable
     */
    public function modifier($key, $callable)
    {
        throw new \BadMethodCallException('Not currently implemented.');
//        $this->modifiers[$key] = $callable;
//        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $method = strtolower($method);
        $this->$method();
        return $this;
    }

    /**
     * Set as async request
     *
     * @param callable|null $success
     * @param callable|null $failure
     * @return $this
     */
    public function async(callable $success = null, callable $failure = null)
    {
        $this->async = true;
        $this->async_funcs['success'] = $success;
        $this->async_funcs['failure'] = $failure;
        return $this;
    }

    /**
     * @return string
     */
    public function buildUri()
    {
        $qs = $this->getParams(self::QUERYSTRING);
        if (count($qs)) {
            return sprintf('%s?%s', $this->getEndpoint(), http_build_query($qs, null, '&'));
        }
        return $this->getEndpoint();
    }

    /**
     * @param null $type
     * @return array
     */
    public function getParams($type = null)
    {
        $this->ensureTypedParams();
        if (is_null($type)) {
            $type = $this->getDefaultParamType();
        }
        $params = [];
        foreach ($this->params as $k => $data) {
            if ($data['type'] === $type || is_null($data['type'])) {
                $params[$k] = $data['value'];
            }
        }
        return $params;
    }

    /**
     * make sure that the params are where they belong
     * set nulls to default and check any that are set to make sure
     * they can belong to the type (eg, body doesn't exist on a get request)
     */
    protected function ensureTypedParams()
    {
        $default_type = $this->getDefaultParamType();
        foreach ($this->params as $key => $data) {
            if (is_null($data['type'])) {
                $this->params[$key]['type'] = $default_type;
            }
            if ($default_type === self::QUERYSTRING && $data['type'] === self::BODY) {
                $this->params[$key]['type'] = self::QUERYSTRING;
            }
        }
    }

    /**
     * @return int
     */
    public function getDefaultParamType()
    {
        return $this->default_param_type;
    }

    /**
     * @return string
     * @throws RequestException
     */
    public function getEndpoint()
    {
        if (!$this->endpoint) {
            throw new RequestException('Endpoint must be provided.');
        }
        return $this->endpoint;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * ['json' => params] or ['form_params' => params] or null if no body
     *
     * @return array|null
     */
    public function getBody()
    {
        if ($this->default_param_type === self::BODY) {
            $params = $this->getParams(self::BODY);
            if ($params) {
                if ($this->getJson()) {
                    return ['json', $params];
                } else {
                    return ['form_params', $params];
                }
            }
        }
        return null;
    }

    /**
     * @return bool
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->request_options;
    }

    /**
     * @return \Smorken\Rest\Contracts\Rest\Model|null
     */
    public function first()
    {
        $this->type = 'first';
        $results = $this->run();
        if ($results instanceof PromiseInterface) {
            return $results;
        }
        return $this->firstFromResults($results);
    }

    /**
     * @return array<array>|null
     */
    public function run()
    {
        if ($this->verify()) {
            $this->setElapsed('start');
            list($results, $last) = $this->getResultsAndLast();
            return $this->fromResult($results, $last);
        }
        return null;
    }

    protected function verify()
    {
        if (!$this->getEndpoint() && !$this->getUseLast()) {
            throw new RequestException('No endpoint provided.');
        }
        if (!$this->getModel()) {
            throw new RequestException('Model must be set.');
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getUseLast()
    {
        return $this->last;
    }

    /**
     * @return \Smorken\Rest\Contracts\Rest\Model
     * @throws ResponseException
     */
    public function getModel()
    {
        if (!$this->model) {
            throw new \UnexpectedValueException("Model must be set.");
        }
        return $this->model;
    }

    public function setModel(\Smorken\Model\Contracts\Model $model)
    {
        $this->model = $model;
        return $this;
    }

    protected function setElapsed($key)
    {
        $this->elapsed[$key] = microtime(true);
    }

    protected function getResultsAndLast()
    {
        $last = $this->getUseLast();
        if ($last) {
            $this->setElapsed('client');
            return [$last, $last];
        }
        $client = $this->getClient();
        $this->setElapsed('client');
        $results = $client->handle($this->toRequestOptions());
        return [$results, $client->debugLastRestRequest()];
    }

    public function getClient()
    {
        if (!$this->client) {
            $cls = $this->getClientClass();
            $this->client = new $cls($this->getClientOptions());
        }
        if ($this->saveLast) {
            $this->client->saveLast();
        }
        $this->client->async($this->getAsync());
        return $this->client;
    }

    /**
     * @return string
     */
    public function getClientClass()
    {
        return $this->clientClass;
    }

    public function getClientOptions()
    {
        return $this->client_options;
    }

    /**
     * Is request async?
     *
     * @return boolean
     */
    public function getAsync()
    {
        return $this->async;
    }

    protected function fromResult($results, $last = null)
    {
        if ($this->saveLast) {
            $this->getModel()
                 ->setLastRestRequest($last);
        }
        if ($this->getAsync() || $results instanceof PromiseInterface) {
            return $this->handleAsync($results);
        }
        $results = $this->ensureResults($results);
        $this->setElapsed('request');
        $results = $this->parseResultsWithParsers($results);
        $results = $this->parseResultsForKey($results);
        $this->setElapsed('total');
        $this->getModel()
             ->setElapsed($this->elapsed);
        return $results;
    }

    /**
     * @param \GuzzleHttp\Promise\PromiseInterface $promise
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    protected function handleAsync(PromiseInterface $promise)
    {
        $this->async = false;
        $success = $this->async_funcs['success'];
        if (is_null($success)) {
            $success = $this->defaultSuccessFunction();
        }
        $failure = $this->async_funcs['failure'];
        if ($success && is_null($failure)) {
            $failure = $this->defaultFailureFunction();
        }
        return $promise->then($success, $failure);
    }

    protected function defaultSuccessFunction()
    {
        return function (ResponseInterface $response) {
            $client = $this->getClient();
            $client->addResponseToLast($response);
            $results = (string)$response->getBody();
            $results = $this->fromResult($results, $client->debugLastRestRequest());
            if ($this->type === 'first') {
                return $this->firstFromResults($results);
            }
            if ($this->type === 'all') {
                return $this->allFromResults($results);
            }
            return $results;
        };
    }

    protected function firstFromResults($results)
    {
        if ($results && is_array($results)) {
            $result = array_shift($results);
            if ($result) {
                return $this->getModel()
                            ->newInstance($result);
            }
        }
        return null;
    }

    protected function allFromResults($results)
    {
        $collection = new Collection();
        if (is_array($results)) {
            foreach ($results as $result) {
                $collection->push(
                    $this->getModel()
                         ->newInstance($result)
                );
            }
        }
        return $collection;
    }

    protected function defaultFailureFunction()
    {
        return function (\GuzzleHttp\Exception\RequestException $e) {
            throw $e;
        };
    }

    protected function ensureResults($results)
    {
        if (is_null($results) || $results === '') {
            return [];
        }
        return $results;
    }

    protected function parseResultsWithParsers($results)
    {
        $parsers = $this->getParsers();
        if ($parsers) {
            foreach ($parsers as $parser) {
                $opts = $this->getParserOptions($parser);
                if ($opts && method_exists($parser, 'setOptions')) {
                    $parser->setOptions($opts);
                }
                $results = $parser->parse($results);
            }
        }
        return $results;
    }

    protected function getParserOptions($parser)
    {
        $p = $parser;
        if (is_object($parser)) {
            $p = get_class($parser);
        }
        return $this->getModel()
                    ->getParserOptions($p);
    }

    protected function getParsers()
    {
        if ($this->getModel()) {
            return $this->getModel()
                        ->getParsers();
        }
    }

    protected function parseResultsForKey($results)
    {
        if ($this->getModel()
                 ->getResultKey()) {
            return $this->findKeyedResults(
                $results,
                $this->getModel()
                     ->getResultKey()
            );
        }
        return $results;
    }

    protected function findKeyedResults($results, $key)
    {
        $parsed = [];
        if (!$results) {
            return $results;
        }
        if (is_array($results)) {
            if (array_key_exists($key, $results)) {
                $parsed[] = $results[$key];
            } else {
                foreach ($results as $k => $v) {
                    if (is_array($v)) {
                        $search = $this->findKeyedResults($v, $key);
                        if ($search) {
                            $parsed[] = $search;
                        }
                    }
                }
            }
        }
        return $parsed;
    }

    /**
     * @return Collection<\Smorken\Rest\Contracts\Rest\Model>
     */
    public function all()
    {
        $this->type = 'all';
        $results = $this->run();
        if ($results instanceof PromiseInterface) {
            return $results;
        }
        return $this->allFromResults($results);
    }

    /**
     * @param \Smorken\Rest\Contracts\Rest\Client $client
     * @return $this
     */
    public function client(\Smorken\Rest\Contracts\Rest\Client $client)
    {
        $this->client = $client;
        return $this;
    }

    public function clientOptions($options)
    {
        $this->client_options = $options;
        return $this;
    }

    protected function addOption($k, $v, &$options)
    {
        if ($v) {
            $options[2][$k] = $v;
        }
    }

    protected function logException(\Exception $e)
    {
        $this->getLogger()
             ->error($e);
    }

    protected function getLogger()
    {
        if (!$this->logger) {
            $this->logger = app(Log::class);
        }
        return $this->logger;
    }

    public function setLogger(Log $logger)
    {
        $this->logger = $logger;
        return $this;
    }

    protected function addModifiers(\Smorken\Rest\Contracts\Rest\Client $client)
    {
        foreach ($this->getModifiers() as $key => $handler) {
            $client->modifier($key, $handler);
        }
    }

    /**
     * @return array
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }
}
