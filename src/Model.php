<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/17
 * Time: 9:54 AM
 */

namespace Smorken\Rest;

use Illuminate\Contracts\Config\Repository;
use Smorken\Rest\Contracts\Rest\Client;
use Smorken\Rest\Parsers\JsonToArray;

class Model extends \Smorken\Model\VO\Model implements \Smorken\Rest\Contracts\Rest\Model
{

    protected static $clientClass;

    /**
     * @var Repository
     */
    protected $config;

    /**
     * @var string
     */
    protected $request_class = \Smorken\Rest\Guzzle\Request::class;

    /**
     * @var array
     */
    protected $parser_class = [JsonToArray::class];

    protected $parser_options = [];

    /**
     * @var string|null
     */
    protected $result_key;
    /**
     * @var array
     */
    protected $last_request;

    protected $client_options = [];

    protected $rest_options = [];

    protected $elapsed = [];

    /**
     * @param $parser_class
     */
    public function addParserClass($parser_class)
    {
        $this->parser_class[] = $parser_class;
    }

    public function setParserClass($parser_class)
    {
        $this->parser_class = [$parser_class];
    }

    /**
     * @return array
     */
    public function getParsers()
    {
        $parsers = [];
        if ($this->parser_class) {
            foreach ($this->parser_class as $parser_class) {
                $parsers[] = new $parser_class;
            }
        }
        return $parsers;
    }

    public function getParserOptions($parser_class)
    {
        if (isset($this->parser_options[$parser_class])) {
            return $this->parser_options[$parser_class];
        }
        return null;
    }

    /**
     * The array key that will bring back the result set for this model
     *
     * @return null|string
     */
    public function getResultKey()
    {
        return $this->result_key;
    }

    /**
     * The array key that will bring back the result set for this model
     *
     * @param $key
     */
    public function setResultKey($key)
    {
        $this->result_key = $key;
    }

    /**
     * @return array|null
     */
    public function getLastRequest()
    {
        return $this->getLastRestRequest();
    }

    /**
     * Sets the last request information on the model
     *
     * @param $data
     */
    public function setLastRequest($data)
    {
        $this->setLastRestRequest($data);
    }

    /**
     * @return array|null
     */
    public function getLastRestRequest()
    {
        return $this->last_request;
    }

    /**
     * @param $data
     */
    public function setLastRestRequest($data)
    {
        $this->last_request = $data;
    }

    /**
     * Get a new request builder for the model.
     *
     * @return \Smorken\Rest\Contracts\Rest\Request
     */
    public function newRequest()
    {
        $client_class = $this->getClientClass();
        $builder = $this->getRequest($client_class);
        $b = $builder->setModel($this)
                     ->clientOptions($this->getClientOptions());
        return $b;
    }

    /**
     * Get the client for the model.
     *
     * @return Client
     */
    public function getClientClass()
    {
        if (!static::$clientClass) {
            $cls = \Smorken\Rest\Guzzle\Client::class;
            static::setClientClass($cls);
        }
        return static::$clientClass;
    }

    /**
     * Set the client factory
     *
     * @param $client_class
     */
    public static function setClientClass($client_class)
    {
        static::$clientClass = $client_class;
    }

    protected function getRequest($client_class)
    {
        return new $this->{'request_class'}($client_class);
    }

    public function getClientOptions()
    {
        if (!$this->client_options) {
            $this->setClientOptions(
                $this->getConfig()
                     ->get('rest.client_options', [])
            );
        }
        return $this->client_options;
    }

    public function setClientOptions($options)
    {
        $this->client_options = $options;
    }

    /**
     * @return Repository
     */
    protected function getConfig()
    {
        if (!$this->config) {
            $this->config = app(Repository::class);
        }
        return $this->config;
    }

    /**
     * @param Repository $config
     */
    public function setConfig(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $key
     * @return int
     */
    public function getElapsed($key = 'total')
    {
        return (float)(array_get($this->elapsed, $key, 0) - array_get($this->elapsed, 'start', 0));
    }

    /**
     * @param array $elapsed
     */
    public function setElapsed(array $elapsed)
    {
        $this->elapsed = $elapsed;
    }
}
