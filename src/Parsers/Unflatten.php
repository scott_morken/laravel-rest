<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 12/6/17
 * Time: 9:34 AM
 */

namespace Smorken\Rest\Parsers;

use Smorken\Rest\Contracts\Rest\Parser;
use Smorken\Rest\Parsers\Traits\ShouldArray;
use Smorken\Rest\ResponseException;

class Unflatten implements Parser
{

    use ShouldArray;

    protected $options = [];

    /**
     * @param $response
     * @return mixed
     * @throws \Smorken\Rest\ResponseException
     */
    public function parse($response)
    {
        if (!$this->options) {
            throw new ResponseException('Unflatten requires options set.');
        }
        if ($this->shouldConvert($response)) {
            $response = $this->convert($response);
        }
        return $this->unflatten(
            $response,
            $this->getIdentifierFromOpts($this->options),
            $this->getChildrenFromOpts($this->options)
        );
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    protected function unflatten($data, $identifier, $child_opts, $into = [])
    {
        if (!$data) {
            return $into;
        }
        foreach ($data as $i => $current) {
            $id = $this->getId($current, $identifier);
            if ($id) {
                if (!array_key_exists($id, $into)) {
                    $into[$id] = $current;
                }
                $into[$id] = array_merge($into[$id], $this->addChildren($data, $id, $identifier, $child_opts));
            }
        }
        return $into;
    }

    protected function addChildren($data, $parent_id, $identifier, $child_opts)
    {
        $rows = [];
        foreach ($data as $row) {
            $id = $this->getId($row, $identifier);
            if ($id && $id === $parent_id) {
                $rows[] = $row;
            }
        }
        $into = [];
        foreach ($child_opts as $key => $options) {
            $into[$key] = [];
            $into[$key] = $this->unflatten(
                $rows,
                $this->getIdentifierFromOpts($options),
                $this->getChildrenFromOpts($options),
                $into[$key]
            );
        }
        return $into;
    }

    protected function getChildrenFromOpts($options)
    {
        if (isset($options['children'])) {
            return $options['children'];
        }
        return [];
    }

    protected function getIdentifierFromOpts($options)
    {
        if (isset($options['identifier'])) {
            return $options['identifier'];
        }
        return null;
    }

    protected function getId($row, $identifier)
    {
        if (!$identifier) {
            return null;
        }
        if (!is_array($identifier)) {
            $identifier = (array)$identifier;
        }
        $key = [];
        $keyed = true;
        foreach ($identifier as $i) {
            $id = $this->getIdFromData($row, $i);
            if ($id) {
                $key[] = $id;
            } else {
                $keyed = false;
            }
        }
        return $keyed && $key ? implode('', $key) : null;
    }

    protected function getIdFromData($row, $identifier)
    {
        if ($row && isset($row[$identifier])) {
            return $row[$identifier];
        }
        return null;
    }
}