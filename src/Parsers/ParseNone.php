<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/7/17
 * Time: 2:32 PM
 */

namespace Smorken\Rest\Parsers;

use Smorken\Rest\Contracts\Rest\Parser;

class ParseNone implements Parser
{

    /**
     * @param $response
     * @return array<array>|null
     */
    public function parse($response)
    {
        return $response;
    }
}
