<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/19/17
 * Time: 8:34 AM
 */

namespace Smorken\Rest\Parsers;

use Smorken\Rest\Contracts\Rest\Parser;
use Smorken\Rest\Parsers\Traits\ShouldArray;

class LovWithValues implements Parser
{

    use ShouldArray;

    /**
     * @param $response
     * @return mixed
     */
    public function parse($response)
    {
        if ($this->shouldConvert($response)) {
            $response = $this->convert($response);
        }
        return $this->toArray($response);
    }

    protected function toArray($response)
    {
        if (!$response) {
            return [];
        }
        $lovs = $this->getLovsFromBase($response);
        $values = $this->getValuesFromLovs($lovs);
        return $this->valuesIntoArray($values);
    }

    protected function getLovsFromBase($response)
    {
        $lovs = [];
        if (!$response) {
            return [];
        }
        if (is_array($response)) {
            if (isset($response['LOVS'])) {
                $t = $response['LOVS'];
                $lovs = $this->getLovsFromLOVS($t);
            } elseif (isset($response['LOV'])) {
                $lovs = $this->getLovsFromLOVS($response);
            } else {
                foreach ($response as $k => $v) {
                    $lovs += $this->getLovsFromBase($v);
                }
            }
        }
        return $lovs;
    }

    protected function getLovsFromLOVS($lovs)
    {
        if (isset($lovs['LOV'])) {
            $t = $lovs['LOV'];
            if (isset($t['VALUES'])) {
                return [$t];
            }
            if (is_array($t)) {
                return $t;
            }
        }
        return [];
    }

    protected function getValuesFromLovs($lovs)
    {
        $values = [];
        foreach ($lovs as $i => $lov) {
            if (isset($lov['VALUES'])) {
                $val = $this->getValueFromValues($lov['VALUES']);
                if (isset($lov['_name'])) {
                    $values[$lov['_name']] = $val;
                } elseif (isset($lov['name'])) {
                    $values[$lov['name']] = $val;
                } else {
                    $values['_no_name_' . $i] = $val;
                }
            }
        }
        return $values;
    }

    protected function getValueFromValues($values)
    {
        $v = [];
        if (isset($values['VALUE'])) {
            $v[] = $values['VALUE']['CODE'];
        } elseif (is_array($values)) {
            foreach ($values as $valuessub) {
                $t = $this->getValueFromValues($valuessub);
                $v = array_merge($v, $t);
            }
        }
        return $v;
    }

    protected function valuesIntoArray($values)
    {
        $results = [];
        foreach ($values as $name => $data) {
            foreach ($data as $index => $value) {
                $results[$index][$name] = $value;
            }
        }
        return $results;
    }
}
