<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/29/17
 * Time: 1:15 PM
 */

namespace Smorken\Rest\Parsers\Traits;

use Smorken\Rest\Parsers\JsonToArray;

trait ShouldArray
{

    protected function shouldConvert($results)
    {
        if (!$results || is_array($results)) {
            return false;
        } else {
            return true;
        }
    }

    protected function convert($results)
    {
        $c = new JsonToArray();
        return $c->parse($results);
    }
}
