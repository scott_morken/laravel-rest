<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 8:36 AM
 */

namespace Smorken\Rest\Parsers;

use Smorken\Rest\Contracts\Rest\Parser;
use Smorken\Rest\ResponseException;

class JsonToArray implements Parser
{

    /**
     * @param $response
     * @return mixed
     */
    public function parse($response)
    {
        $arr = json_decode($response, true);
        if ($response && (is_null($arr) || $arr === false)) {
            throw new ResponseException("Unable to convert response to JSON: $response");
        }
        return $arr;
    }
}