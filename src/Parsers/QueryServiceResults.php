<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/27/17
 * Time: 8:16 AM
 */

namespace Smorken\Rest\Parsers;

use Smorken\Rest\Contracts\Rest\Parser;
use Smorken\Rest\Parsers\Traits\ShouldArray;

class QueryServiceResults implements Parser
{

    use ShouldArray;

    /**
     * @param $response
     * @return mixed
     */
    public function parse($response)
    {
        if ($this->shouldConvert($response)) {
            $response = $this->convert($response);
        }
        $rows = $this->getRows($response);
        return $this->verifyRowsIsArray($rows);
    }

    protected function getRows($response)
    {
        if (isset($response['rows'])) {
            return $response['rows'];
        }
        $rows = [];
        foreach ($response as $k => $v) {
            if (is_array($v)) {
                if (isset($v['rows'])) {
                    $rows = $rows + $v['rows'];
                } else {
                    $rows = $rows + $this->getRows($v);
                }
            }
        }
        return $rows;
    }

    protected function verifyRowsIsArray($rows)
    {
        foreach ($rows as $id => $row) {
            if (!is_array($row)) {
                return [$rows];
            }
        }
        return $rows;
    }
}
