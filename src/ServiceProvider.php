<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 3:25 PM
 */

namespace Smorken\Rest;

use Phpro\SoapClient\ClientFactory;
use Phpro\SoapClient\ClientFactoryInterface;
use Smorken\Rest\Contracts\Rest\PromiseHandler;
use Smorken\Rest\Contracts\Type\ArrayToXml;
use Smorken\Rest\Contracts\Type\XmlToArray;
use Smorken\Rest\Guzzle\Promises\Handler;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->bootConfig();
        \Smorken\Rest\Model::setClientClass(\Smorken\Rest\Guzzle\Client::class);
    }

    protected function bootConfig()
    {
        $config = __DIR__ . '/../config/config.php';
        $this->mergeConfigFrom($config, 'rest');
        $this->publishes([$config => config_path('rest.php')], 'config');
    }

    public function register()
    {
        $this->bindPromiseHandler();
    }

    protected function bindPromiseHandler()
    {
        $this->app->bind(
            PromiseHandler::class,
            function ($app) {
                return new Handler();
            }
        );
    }
}
