<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/17
 * Time: 9:25 AM
 */

namespace Smorken\Rest;

class RequestMethodException extends RequestException implements \Smorken\Rest\Contracts\RequestMethodException
{

}